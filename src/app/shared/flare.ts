export const FLAREW = {
        name: "Precandidatos",
        children: [
        {
            name: "Precandidatos Tipo 1",
            children: [
                {
                    name: "PT1 1",
                    size: 100
                }
            ]
        },
        {
            name: "Precandidatos Tipo 2",
            children: [
                {
                    name: "PT2 1",
                    size: 100
                },
                {
                    name: "PT2 2",
                    children: [
                        {
                            name: "PT22 1",
                            size: 100
                        },
                        {
                            name: "PT22 2",
                            size: 200
                        }
                    ]
                }
            ]
        },
        {
            name: "Precandidatos Tipo 3",
            children: [
                {
                    name: "PT3 1",
                    size: 100
                },
                {
                    name: "PT3 2",
                    size: 200
                },
                {
                    name: "PT3 3",
                    children: [
                        {
                            name: "PT33 1",
                            size: 100
                        },
                        {
                            name: "PT33 2",
                            size: 200
                        },
                        {
                            name: "PT33 3",
                            size: 300
                        }
                    ]
                }
            ]
        },
        {
            name: "Precandidatos Tipo 4",
            children: [
                {
                    name: "PT4 1",
                    size: 100
                },
                {
                    name: "PT4 2",
                    size: 200
                },
                {
                    name: "PT4 3",
                    size: 300
                },
                {
                    name: "PT4 4",
                    children: [
                        {
                            name: "PT44 1",
                            size: 100
                        },
                        {
                            name: "PT44 2",
                            size: 200
                        },
                        {
                            name: "PT44 3",
                            size: 300
                        },
                        {
                            name: "PT44 4",
                            size: 400
                        }
                    ]
                }
            ]
        }
    ]
};