export const POPULATION: any[] = [
    {age: '<5', population: 1},
    {age: '5-13', population: 1},
    {age: '14-17', population: 1},
    {age: '18-24', population: 1},
    {age: '25-44', population: 1},
    {age: '45-64', population: 1},
    {age: '≥65', population: 1}
];
