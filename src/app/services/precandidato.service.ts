import {Injectable} from '@angular/core';
import {Precandidatos} from "../precandidato/precandidatos";
import {Observable} from 'rxjs';
import {Global} from '../global';
import {HttpClient} from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class PrecandidatosService {
    private baseURL;

    constructor(private http: HttpClient) {
        this.baseURL = Global.url;
    }

    getPrecandidatos(){
        return this.http.get(this.baseURL + 'precandidatos');
    }

    guardarPrecandidato(precandidato: Precandidatos, idRrhh) {
        return this.http.post(this.baseURL + 'precandidatos/' + idRrhh, precandidato);
    }

    validarCurp(curp) {
        return this.http.get(this.baseURL + 'precandidatos/' + curp)
    }
}
