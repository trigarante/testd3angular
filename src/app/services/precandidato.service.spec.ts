import { TestBed } from '@angular/core/testing';

import { PrecandidatosService } from './precandidato.service';

describe('PrecandidatosService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: PrecandidatosService = TestBed.get(PrecandidatosService);
        expect(service).toBeTruthy();
    });
});
