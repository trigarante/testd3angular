import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';

import { POPULATION } from '../shared';

@Component({
    selector: 'app-donut-chart',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './donut-chart.component.html',
    styleUrls: ['./donut-chart.component.css']
})
export class DonutChartComponent implements OnInit {

    title = 'Donut Chart';

    private width: number;
    private height: number;

    private svg: any;     // TODO replace all `any` by the right type

    private radius: number;

    private arc: any;
    private pie: any;
    private color: any;

    private g: any;

    constructor() {}

    ngOnInit() {
        this.initSvg();
        this.drawChart(POPULATION);
        this.drawLegend(POPULATION);
    }

    private initSvg() {
        this.svg = d3.select('svg');

        this.width = +this.svg.attr('width');
        this.height = +this.svg.attr('height');
        this.radius = Math.min(this.width, this.height) / 2;

        this.color = d3Scale.scaleOrdinal()
            .range(['#98abc5', '#8a89a6', '#7b6888', '#6b486b', '#a05d56', '#d0743c', '#ff8c00']);

        this.arc = d3Shape.arc()
            .outerRadius(this.radius - 10)
            .innerRadius(this.radius - 170);

        this.pie = d3Shape.pie()
            .sort(null)
            .value((d: any) => d.population);

        this.svg = d3.select('svg')
            .append('g')
            .attr('transform', 'translate(' + this.width / 2 + ',' + this.height / 2 + ')');
    }

    private drawChart(data: any[]) {
        const g = this.svg.selectAll('.arc')
            .data(this.pie(data))
            .enter().append('g')
            .attr('class', 'arc');

        g.append('path')
            .attr('d', this.arc)
            .style('fill', d => this.color(d.data.age));

        g.append('text')
            .attr('transform', d => 'translate(' + this.arc.centroid(d) + ')')
            .attr('dy', '.35em')
            .text(d => d.data.population);
    }
    private drawLegend(data: any[]){
        let legend = this.svg.append('g')
            .attr('font-family', 'sans-serif')
            .attr('font-size', 12)
            .attr('text-anchor', 'end')
            .selectAll('g')
            .data(this.pie(data))
            .enter().append('g')
            .attr('transform', (d, i) => 'translate(0,' + ((i * 20)-200) + ')');

        legend.append('rect')
            .attr('x', this.width/2 - 19)
            .attr('width', 19)
            .attr('height', 19)
            .attr('fill', d => this.color(d.data.age));

        legend.append('text')
            .attr('x', this.width/2 - 50)
            .attr('y', 9.5)
            .attr('dy', '0.32em')
            .text(d => d.data.age);
    }
}
