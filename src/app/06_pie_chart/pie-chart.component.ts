import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3ScaleChromatic from 'd3-scale-chromatic';
import * as d3Partition from 'd3-hierarchy';
import * as d3Path from 'd3-path';
import * as d3Format from 'd3-format';
import * as d3Inter from 'd3-interpolate';
import 'd3-transition';

import {FLAREW} from '../shared';
import {PrecandidatosService} from '../services/precandidato.service';
import {Precandidatos} from '../precandidato/precandidatos';
import {Fathers} from '../interfaces/fathers';
import {Childs} from '../interfaces/childs';

@Component({
    selector: 'app-pie-chart',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './pie-chart.component.html',
    styleUrls: ['./pie-chart.component.css'],
    providers: [PrecandidatosService]
})


export class PieChartComponent implements OnInit {

    constructor(private precandidatosService: PrecandidatosService) {
        this.width = 900 - this.margin.left - this.margin.right;
        this.height = 500 - this.margin.top - this.margin.bottom;
        this.radius = Math.min(this.width, this.height) / 2;
        this.maxRadius = (Math.min(this.width, this.height) / 2) - 5;
        this.flares = {
            name: 'Precandidato',
            children: []
        };

    }

    title = 'Prueba';
    precandidatos: any;
    precandidatos1: any[];
    precandidatos2: any[];
    cp1: number;
    cp2: number;
    flares: Fathers;
    childs: Childs;
    private margin = {top: 20, right: 20, bottom: 30, left: 50};
    private width: number;
    private height: number;
    private radius: number;
    private maxRadius: number;
    private quantize: any;
    x;
    y;
    partition;
    formatNumber;
    root;
    private arc: any;
    private color: any;
    private svg: any;

    ngOnInit() {
        this.initSvg();
        this.getPrecandidatos();
    }
    getPrecandidatos() {
        this.precandidatosService.getPrecandidatos()
            .subscribe(data => {
                this.precandidatos = data;
                this.drawPie(this.precandidatos);
            });
    }

    private initSvg() {

        const colors = ['#666699', '#cc3300', '#5c85d6'];
        this.formatNumber = d3Format.format(',d');
        this.color = d3Scale.scaleOrdinal(colors);
            // d3ScaleChromatic.schemeCategory10);
        this.x = d3Scale.scaleLinear()
            .range([0, 2 * Math.PI])
            .clamp(true);

        this.y = d3Scale.scaleSqrt()
            .range([this.maxRadius * .1, this.maxRadius]);

        this.partition = d3Partition.partition();


        this.arc = d3Shape.arc()
            .startAngle( d => this.x(d.x0))
            .endAngle(d => this.x(d.x1))
            .innerRadius(d => Math.max(0, this.y(d.y0)))
            .outerRadius(d => Math.max(0, this.y(d.y1)));

    }

    private drawPie(data) {
            // this.childs = {
            //     name: 'Precandidatos 1',
            //     size: data.length
            // };

        // this.flares.children.push(this.childs);
        this.root = d3Partition.hierarchy(this.dataTransform(data));
        this.root.sum(d => d.size);
        this.root.each(d => d.current = d);
        this.svg = d3.select('svg')
            .append('g')
            .attr('transform', 'translate(' + this.width / 2 + ',' + this.height / 2 + ')')
            .on('click', () => this.focusOn());
        const slice = this.svg.selectAll('g.slice')
            .data(this.partition(this.root).descendants());
        slice.exit().remove();
        const newSlice = slice.enter()
            .append('g').attr('class', 'slice')
            .on('click', d => {
                d3.event.stopPropagation();
                this.focusOn(d);
            });

        newSlice.append('title')
            .text(d => d.data.name + '\n' + this.formatNumber(d.value));

        newSlice.append('path')
            .attr('class', 'main-arc')
            .attr('fill', d => { while (d.depth > 1) { d = d.parent; } return this.color(d.data.name); })
            .attr('fill-opacity', d => d.children ? 0.6 : 0.4 )
            .attr('d', this.arc);

        newSlice.append('path')
            .attr('class', 'hidden-arc')
            .attr('id', (_, i) => 'hiddenArc' + i)
            .attr('d', d => this.middleArcLine(d));

        const text = newSlice.append('text')
            .attr('display', d => this.textFits(d) ? null : 'none');

        text.append('textPath')
            .attr('startOffset', '50%')
            .attr('xlink:href', (_, i) => '#hiddenArc' + i)
            .text(d => d.data.name)
            .style('fill', d => this.color(d.data.name))
            .style('fill-opacity', 0.3);
        text.append('textPath')
            .attr('startOffset', '50%')
            .attr('xlink:href', (_, i) => '#hiddenArc' + i)
            .text(d => d.data.name)
            .style('fill', '#5b5b5b');

    }

    private focusOn(d = { x0: 0, x1: 1, y0: 0, y1: 1 }) {

        const transition = this.svg.transition()
        .duration(750)
        .tween('scale', () => {
        const xd = d3Inter.interpolate(this.x.domain(), [d.x0, d.x1]),
        yd = d3Inter.interpolate(this.y.domain(), [d.y0, 1]);
        return t => { this.x.domain(xd(t)); this.y.domain(yd(t)); };
        });

        transition.selectAll('path.main-arc')
        .attrTween('d', d => () => this.arc(d));

        transition.selectAll('path.hidden-arc')
        .attrTween('d', d => () => this.middleArcLine(d));

        transition.selectAll('text')
        .attrTween('display', d => () => this.textFits(d) ? null : 'none');

    }

    private textFits(d) {
        const CHAR_SPACE = 6;
        const deltaAngle = this.x(d.x1) - this.x(d.x0);
        const r = Math.max(0, (this.y(d.y0) + this.y(d.y1)) / 2);
        const perimeter = r * deltaAngle;

        return d.data.name.length * CHAR_SPACE  <  perimeter;
    }

    private middleArcLine (d) {
        const halfPi = Math.PI / 2;
        const angles = [this.x(d.x0) - halfPi, this.x(d.x1) - halfPi];
        const r = Math.max(0, (this.y(d.y0) + this.y(d.y1)) / 2);

        const middleAngle = (angles[1] + angles[0]) / 2;
        const invertDirection = middleAngle > 0 && middleAngle  <  Math.PI; // On lower quadrants write text ccw
        if (invertDirection) {
            angles.reverse();
        }

        const path = d3Path.path();
        path.arc(0, 0, r, angles[0], angles[1], invertDirection);
        return path.toString();
    }

    private dataTransform(data: Precandidatos[]) {
        const actvios = {
            name: 'Precandidatos Activos',
            children: []
        };
        const precandidatosActivos = [];
        const precandidatosBaja = [];
        const paCandidato = [];
        const paCapacitacion = [];
        const paSeguimiento1 = [];
        const paSeguimiento2 = [];
        const paEmpleado = [];
        const pbCandidato = [];
        const pbCapacitacion = [];
        const pbSeguimiento1 = [];
        const pbSeguimiento2 = [];
        const pbEmpleado = [];

        // const candidatos
        for (let i = 0; i < data.length ; i++) {
            if (data[i].idEstado == 1) {        // Precancidatos Activos
               precandidatosActivos.push(data[i]);
                if (data[i].idEtapa == 1) {  // Precandidato
                // paPrecandidato.push(data[i]);
                } else if (data[i].idEtapa == 2) {  // Candidato
                paCandidato.push(data[i]);
                } else if (data[i].idEtapa == 3) {  // Empleado
                paEmpleado.push(data[i]);
                } else if (data[i].idEtapa == 4) {  // Capacitación
                paCapacitacion.push(data[i]);
                } else if (data[i].idEtapa == 5) {  // Seguimiento 1
                paSeguimiento2.push(data[i]);
                } else if (data[i].idEtapa == 6) {  // Seguimiento 2
                paEmpleado.push(data[i]);
                }
            } else {        // Precancidatos Inactivos
                if (data[i].idEtapa == 1) {  // Precandidato
                    // paPrecandidato.push(data[i]);
                } else if (data[i].idEtapa == 2) {  // Candidato
                    pbCandidato.push(data[i]);
                } else if (data[i].idEtapa == 3) {  // Empleado
                    pbEmpleado.push(data[i]);
                } else if (data[i].idEtapa == 4) {  // Capacitación
                    pbCapacitacion.push(data[i]);
                } else if (data[i].idEtapa == 5) {  // Seguimiento 1
                    pbSeguimiento2.push(data[i]);
                } else if (data[i].idEtapa == 6) {  // Seguimiento 2
                    pbEmpleado.push(data[i]);
                }
            }
        }
        const activos = {
            name: 'Precandidatos Activos',
            children: []
        };

        const bajas = {
            name: 'Precandidatos Baja',
            children: []
        };
        const paCandi = {
            name: 'Candidato',
            size: paCandidato.length
        };
        const paCapa = {
            name: 'Capacitacion',
            size: paCapacitacion.length
        };
        const paSegui1 = {
            name: 'Seguimiento 1',
            size: paSeguimiento1.length
        };
        const paSegui2 = {
            name: 'Seguimiento 2',
            size: paSeguimiento2.length
        };
        const paEmple = {
            name: 'Empleado',
            size: paEmpleado.length
        };
        const pbCandi = {
            name: 'Candidato',
            size: pbCandidato.length
        };
        const pbCapa = {
            name: 'Capacitacion',
            size: pbCapacitacion.length
        };
        const pbSegui1 = {
            name: 'Seguimiento 1',
            size: pbSeguimiento1.length
        };
        const pbSegui2 = {
            name: 'Seguimiento 2',
            size: pbSeguimiento2.length
        };
        const pbEmple = {
            name: 'Empleado',
            size: pbEmpleado.length
        };
        activos.children.push(paCandi);
        activos.children.push(paCapa);
        activos.children.push(paSegui1);
        activos.children.push(paSegui2);
        activos.children.push(paEmple);
        bajas.children.push(pbCandi);
        bajas.children.push(pbCapa);
        bajas.children.push(pbSegui1);
        bajas.children.push(pbSegui2);
        bajas.children.push(pbEmple);

        this.flares.children.push(bajas);
        this.flares.children.push(activos);
        return this.flares;
    }

    private drawLegend(data: any) {
        this.root = d3Partition.hierarchy(data);
        const legend = this.svg.append('g')
            .attr('font-family', 'sans-serif')
            .attr('font-size', 12)
            .attr('text-anchor', 'end')
            .append('g')
            .attr('transform', (d, i) => 'translate(0,' + ((i * 20) - 200) + ')')
            .on('click', () => this.focusOn());

        const slice = legend.selectAll('g.slice')
            .data(this.partition(this.root).descendants());

        slice.exit().remove();

        const newSlice = slice.enter()
            .append('g').attr('class', 'slice')
            .on('click', d => {
                d3.event.stopPropagation();
                d.children ? this.focusOn(d) : '';
            });

        newSlice.append('rect')
            .attr('x', this.width / 2 - 19)
            .attr('width', 19)
            .attr('height', 19)
            .attr('fill', d => this.color(d.data.name))
            .attr('fill-opacity', d => d.children ? 0.6 : 0.4 );

        newSlice.append('text')
            .attr('x', this.width / 2 - 50)
            .attr('y', 9.5)
            .attr('dy', '0.32em')
            .text(d => d.data.name);
    }
}
